# Weather App
Application météo avec API en Angular

## Objectifs :
- Créer une application mobile de météo
- Dialoguer avec une API retournant les données météorologique suivant une localité en JSON
- Intégrer une API de suggestion de lieux sur un input
