import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { WeatherService } from '../services/weather.service';
declare let places: any;

@Component({
    selector: 'app-weather',
    templateUrl: './weather.component.html',
    styleUrls: ['./weather.component.scss']
})
export class WeatherComponent implements OnInit {

    city = 'Grenoble';

    cityName = new FormControl(this.city);
    autocomplete = null;
    data = null;
    countryCode = 'fr';

    imgSrc = {
        Rain: "./assets/img/rain.svg",
        Clear: "./assets/img/sun.svg",
        Clouds: "./assets/img/clouds.svg"
    }

    constructor(private weatherService: WeatherService) { }

    ngOnInit() {
        this.weatherService.getWeather(this.city, this.countryCode).subscribe(
            (apiData) => {
                this.data = apiData;
                console.log(this.data);

            }
        );
        this.autocomplete = places({
            appId: 'plB22YC2I3A2',
            apiKey: 'cdf782aa20f125edc39f31f7bbe619b5',
            container: document.querySelector('#address-input')
        });
        var suggestion = null;
        this.autocomplete.on('change', (e)=>{
            this.city = e.suggestion.name
            this.countryCode = e.suggestion.countryCode;
            console.log(e.suggestion)
            console.log(this.city)
        });
    }

    onSearch(){
        this.weatherService.getWeather(this.city, this.countryCode).subscribe(
            (apiData) => {
                this.data = apiData;
                console.log(this.data);

            }
        );
    };



}
